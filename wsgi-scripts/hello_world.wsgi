from cgi import parse_qs
def application(environ, start_response):

	status = '200 OK'

	output = 'Hello World! \n'

	GET = parse_qs(environ['QUERY_STRING'])

	output += "GET params: \n"
	for key in GET:
		output += key+"=" + GET[key][0]+ "; "
	request_body_size = int(environ.get('CONTENT_LENGTH', 0))
	output += "\n"
	request_body = environ['wsgi.input'].read(request_body_size)
	POST = parse_qs(request_body)
	output += "POST params: \n"
	for key in POST:
		output += key+"=" + POST[key][0]+ "; "

	
	response_headers = [('Content-type', 'text/plain'),
		        ('Content-Length', str(len(output)))]

	start_response(status, response_headers)
	return [output]
