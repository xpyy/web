from django.conf.urls import patterns, url
from ask import views

urlpatterns = patterns('',
    url(r'^helloworld/', views.helloworld, name='helloworld'),
    url(r'^$', views.index,),
    url(r'^question/(\d+)', views.question),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^accounts/profile/$', views.profile, name='profile'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': 'request.path'}, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^ask/$', views.ask, name='ask'),
    url(r'^answer/(\d+)', views.answer),
    url(r'^search/$', views.search,),
)
