from django.db import models
from django.contrib.auth.models import User
from djangosphinx.models import SphinxSearch
from django.core.files.storage import FileSystemStorage

# Create your models here
class Profile(models.Model):
    user = models.OneToOneField(User)
    rating = models.IntegerField(default=0)
    avatar = models.CharField(max_length=60)
    
User.profile = property(lambda u: Profile.objects.get_or_create(user=u)[0])
    
class Tags(models.Model):
    tag = models.CharField(max_length=20)   
   
class Question(models.Model):
    author = models.ForeignKey(Profile)
    title = models.CharField(max_length=60)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    rating = models.IntegerField(default=0)
    tag_list = models.ManyToManyField(Tags)
    
    
    search = SphinxSearch(
        index = 'ask_question_idx', 
        weights = { 'title':100, 'text':20 },
    )

class Answer(models.Model):
    author = models.ForeignKey(Profile)
    question = models.ForeignKey(Question)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    is_right = models.BooleanField(default=False)
    
class Image(models.Model):
    docfile = models.ImageField("Image", upload_to='image/%Y/%m/%d')
