# coding=utf-8
from django.core.management.base import BaseCommand
import sys
from mixer.fakers import get_username, get_lorem, get_email, get_firstname, get_lastname
from mixer.generators import get_datetime
import random
import hashlib
import csv
 
reload(sys)
sys.setdefaultencoding('utf-8')
 
def generateUser():
    password = "12345"
    date = str(get_datetime(min_datetime=(2014, 10, 1, 0, 0, 0), max_datetime=(2014, 11, 17, 23, 59, 59)))
    is_super = 0
    first_name = get_firstname()
    last_name = get_lastname()
    user_name = get_username()
    email = get_email(user_name)
    is_staff = 0
    is_active = 1 
    return ("", password, date, is_super, user_name, first_name, last_name, email, is_staff, is_active, date)
 
def generateQuestion():
    title = get_lorem(50)
    text = get_lorem(150)
    date_added = str(get_datetime(min_datetime=(2014, 10, 1, 0, 0, 0), max_datetime=(2014, 10, 17, 23, 59, 59)))
    author_id = random.randint(1,9534)
    rating = random.randint(-1000,1000)
    return ("", title, text, date_added, author_id, rating)
 
def generateAnswer():
    text = get_lorem(100)
    date_added = str(get_datetime(min_datetime=(2014, 11, 1, 0, 0, 0), max_datetime=(2014, 11, 17, 23, 59, 59)))
    author = random.randint(1, 9534)
    question = random.randint(1, 10000)
    right = 0
    return ("", text, date_added, right, author, question)
 
def generateTagsToQuestion():
    tagId = random.randint(1, 100)
    questionId = random.randint(1, 10000)
    return ("", questionId, tagId)

def generateTags():
    tag = get_lorem(20)
    return ("", tag)    

def generateProfile(i):
    rating = random.randint(-1000, 1000)
    avatar = "http://lorempixel.com/40/40/"
    user_id = i+1
    return("", rating, avatar, user_id)
 
def writeFakeInformationInCSV(f, *args):
    writer = csv.writer(f, delimiter=",", quotechar='"', quoting=csv.QUOTE_NONNUMERIC, lineterminator='\n')
    writer.writerow(args[0])
 
class Command(BaseCommand):
 
    def handle(self, *args, **options):
        fAns = open("/tmp/answers.csv", "w")
        fQue = open("/tmp/questions.csv", "w")
#        fUse = open("/tmp/users.csv", "w")
        fTtq = open("/tmp/tagsToQuestions.csv", "w")
#       fTag = open("/tmp/tags.csv", "w")
#        fPro = open("/tmp/profiles.csv", "w") 
 
#        for i in xrange(0, 10000):
#            writeFakeInformationInCSV(fUse, generateUser())
            
#       for i in xrange(0, 100):
#           writeFakeInformationInCSV(fTag, generateFake())
 
        for i in xrange(0, 10000):
            writeFakeInformationInCSV(fQue, generateQuestion())
 
        for i in xrange(0, 50000):
            writeFakeInformationInCSV(fAns, generateAnswer())
 
        for i in xrange(0, 20000):
            writeFakeInformationInCSV(fTtq, generateTagsToQuestion())
      
#        for i in xrange(0, 9534):
#            writeFakeInformationInCSV(fPro, generateProfile(i))
            
        fAns.close()
        fQue.close()
#        fUse.close()
        fTtq.close()
#       fTag.close()
#        fPro.close()
