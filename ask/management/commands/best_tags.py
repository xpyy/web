from django.core.management.base import BaseCommand
from django.core.cache import caches
from ask.models import Tags
from django.db.models import Count

memcached = caches['default']

class Command(BaseCommand):
	def handle(self, *args, **kwargs):
		tags = Tags.objects.annotate(count_quest = Count('question')).order_by('-count_quest')[:10]
		
		best_tags = []
		for t in tags:
			best_tags.append( {"tag": t.tag,"count_quest": t.count_quest } )

		memcached.set('best_tags', best_tags, 3600)
