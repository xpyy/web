from django.core.management.base import BaseCommand
from django.core.cache import caches
from ask.models import Question

memcached = caches['default']

class Command(BaseCommand):
	def handle(self, *args, **kwargs):
		questions = Question.objects.all().order_by('-rating')[:10]
		
		best_users = []
		for q in questions:
			best_users.append( {"author": q.author.user,"rating": q.rating } )

		memcached.set('best_users', best_users, 3600)
