# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0005_answer_rating'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answer',
            name='rating',
        ),
    ]
