# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0002_auto_20141118_1301'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answer',
            name='tag_list',
        ),
        migrations.AddField(
            model_name='question',
            name='tag_list',
            field=models.ManyToManyField(to='ask.Tags'),
            preserve_default=True,
        ),
    ]
