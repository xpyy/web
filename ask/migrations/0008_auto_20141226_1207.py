# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0007_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='docfile',
            field=models.ImageField(upload_to=b'image/%Y/%m/%d', verbose_name=b'Image'),
        ),
    ]
