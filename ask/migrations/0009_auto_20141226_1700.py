# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0008_auto_20141226_1207'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profile',
            old_name='avatar_url',
            new_name='avatar',
        ),
    ]
