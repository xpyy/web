from django.contrib.auth.models import User
from django import forms
from ask.models import Question, Answer

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password')
        widgets = {
            'password': forms.PasswordInput(),
        }
        

class QuestionForm(forms.ModelForm):
    
    def  __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(QuestionForm, self).__init__(*args, **kwargs)
        
    def save(self, commit=True):
        obj = super(QuestionForm, self).save(commit=False)
        obj.author_id = self.user
        if commit:
            obj.save()
            return obj
        
    class Meta:
        model = Question
        fields = ('title', 'text')
        
class AnswerForm(forms.ModelForm):
    
    def  __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.question_id = kwargs.pop('question_id', None)
        super(AnswerForm, self).__init__(*args, **kwargs)
        
    def save(self, commit=True):
        obj = super(AnswerForm, self).save(commit=False)
        obj.author_id = self.user
        obj.question_id = self.question_id
        if commit:
            obj.save()
            return obj
        
    class Meta:
        model = Answer
        fields = ('text',)
        
class ProfileForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields['username'].required = False
        self.fields['email'].required = False
        self.fields['password'].required = False

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
        widgets = {
            'password': forms.PasswordInput(),
        }
