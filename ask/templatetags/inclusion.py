from django import template
from ask.models import Question, Tags
from django.db.models import Count
from django.core.cache import caches
register = template.Library()
memcached = caches['default']


@register.inclusion_tag("inc/best_users.html")
def best_users():
    q = memcached.get('best_users')
    return {'questions':q}
    
@register.inclusion_tag("inc/best_tags.html")
def best_tags():
    tags = memcached.get('best_tags')
    return {'tags':tags}
