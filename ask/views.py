from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.paginator import Paginator, EmptyPage
from django.db.models import Count
from ask.models import Question, Answer, User, Image, Tags
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from forms import UserForm, QuestionForm, AnswerForm, ProfileForm
from django.contrib.auth import authenticate, login
from django.utils.http import is_safe_url
from django.contrib.auth import (REDIRECT_FIELD_NAME, login as auth_login, logout as auth_logout, get_user_model, update_session_auth_hash)
from django.core.urlresolvers import reverse
from django.template import RequestContext




def index(request):
    page = request.GET.get('page',1)
    filter = request.GET.get('new','hot')
    if (filter == 'hot'):
        paginator = Paginator(Question.objects.prefetch_related('tag_list').order_by('-rating'), 20)
    else:
        paginator = Paginator(Question.objects.prefetch_related('tag_list').order_by('-date_added'), 20)
    paginator_page = paginator.page(page)
    return render(request, 'index.html',{"question_list":paginator_page})


    
def question(request,q_id):
    questions = Question.objects.prefetch_related('tag_list').get(pk=q_id)
    page = request.GET.get('page',1)
    paginator = Paginator(Answer.objects.all().filter(question_id=q_id).order_by('-date_added'), 5)
    paginator_page = paginator.page(page)
    if request.method == 'POST':
        form = AnswerForm(request.POST, user=request.user.id, question_id=q_id)
        if form.is_valid():
            form.save()
            #url = '/question/' + q_id            
            return render(request,'questionPage.html',{"q" : questions, "answers" : paginator_page})
    else:
        form = AnswerForm()
    return render(request,'questionPage.html',{"q" : questions, "answers" : paginator_page, 'form':form})




@login_required(redirect_field_name='login')
def profile(request):
    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            if form.cleaned_data.get('username') != "":
                request.user.username = form.cleaned_data.get('username')
            if form.cleaned_data.get('email') != "":
                request.user.email = form.cleaned_data.get('email')
            if form.cleaned_data.get('password') != "":
                request.user.password = form.cleaned_data.get('password')
            request.user.save()           

            return HttpResponseRedirect(reverse('ask.views.profile'))
    else:
        form = ProfileForm()
    return render(request, 'profile.html', {'form':form})



@login_required(redirect_field_name='login')
def ask(request):
    if request.method == 'POST':
        form = QuestionForm(request.POST, user=request.user.id)
        if form.is_valid():
            newobject = form.save()
            tags = request.POST.get('tags').replace(" ", "")
            tags_arr = tags.split(',')
            if tags_arr[0]:
				newtags = []
				for tag_text in tags_arr:
					tag = Tags.objects.get_or_create(tag=tag_text)
					newobject.tag_list.add(tag[0])
            url = '/question/' + str(newobject.id)  
            return HttpResponseRedirect(url)
    else:
        form = QuestionForm()
    return render(request, 'ask.html', {'form':form})
    
    
    
@login_required(redirect_field_name='login')
def answer(request,q_id):
    if request.method == 'POST':
        form = AnswerForm(request.POST, user=request.user.id, question_id=q_id)
        if form.is_valid():
            form.save()
            url = '/question/' + q_id            
            return HttpResponseRedirect(url)
    else:
        form = AnswerForm()
    return render(request, 'add_answer.html', {'form':form})
    
    
def signup(request, next='/'):
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            if (REDIRECT_FIELD_NAME in request.POST or REDIRECT_FIELD_NAME in request.GET):
                next = request.POST.get(REDIRECT_FIELD_NAME, request.GET.get(REDIRECT_FIELD_NAME))
            # Security check -- don't allow redirection to a different host.
                if not is_safe_url(url=next, host=request.get_host()):
                    next = request.path
            new_user = User.objects.create_user(**form.cleaned_data)
            new_user = authenticate(username=request.POST['username'],password=request.POST['password'])
            login(request, new_user)
            # redirect, or however you want to get to the main view
            return HttpResponseRedirect(next)
    else:
        form = UserForm() 

    return render(request, 'signup.html', {'form': form})
    
def search(request):
    query = request.GET.get('query')
    page = request.GET.get('page',1)
    paginator = Paginator(Question.search.query(query).order_by('-@weight'), 20)
    paginator_page = paginator.page(page)
    return render(request, 'index.html',{"question_list":paginator_page})

def helloworld(request):
    output = '<html><body>Hello in /helloworld <br>'
    GET = request.GET
    POST = request.POST
    output += "GET params: <br>"
    for key in GET:
        output += key+"=" + GET[key]+ ";<br>"
    output += "POST params: <br>"
    for key in POST:
        output += key+"=" + POST[key]+ ";<br> "
    return HttpResponse([output])
